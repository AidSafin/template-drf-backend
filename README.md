[![pipeline status](https://git.technokratos.com/backend_territory/template-drf-backend/badges/master/pipeline.svg)](https://git.technokratos.com/backend_territory/template-drf-backend/commits/master)
[![coverage report](https://git.technokratos.com/backend_territory/template-drf-backend/badges/master/coverage.svg)](https://git.technokratos.com/backend_territory/template-drf-backend/commits/master)

# Django REST Framework Template API project 

This repo contains a ready-to-use version of a template project for building REST api with Django REST Framework.

## Usage

Follow simple steps:
1) Clone project to the target machine.
2) In PyCharm, press `Ctrl+Shift+R` and replace all occurrences of `template-drf` to your project's name.
3) Rename folder `template-drf` to your project's name. 
4) Project is ready to run.

Try to build and start project (see `Managing` section).
To be sure that project works correctly, also run tests.


<b>Notice</b>: this is a development configuration intended to provide a "fast start" with new project. 
You must reconfigure it to use in stage or production environments.

## Features
* configured django project
    * REST framework attached
    * `users` application (covered by unit tests)
    * [swagger documentation generator](https://drf-yasg.readthedocs.io/en/stable/)
    * commonly used utilities
* [pytest](https://docs.pytest.org/en/latest/) and [coverage](https://coverage.readthedocs.io/en/v4.5.x/) configuration files
* separate requirements file for unit tests
* docker image based on <i>python-3.6:alpine</i>
* docker-compose configuration for local run with following services:
    * adminer
    * postgres
    * api (run with Django server)
* .gitlab-ci configuration
* .gitignore file


## Managing

### Run using docker-compose:

```bash
cd template-drf

docker-compose up -d
docker-compose exec -T template-drf-api python manage.py makemigrations
docker-compose exec -T template-drf-api python manage.py migrate
```

### Run tests with coverage

```bash
cd template-drf

docker-compose exec -T template-drf-api sh -c "coverage run -m py.test --cache-clear && coverage report"
```


## TODO

* configure postgres (build locally from remote image)
* add docker-compose configurations for separate environments (test, stage)
* add configurable nginx service to docker-compose (build locally from remote image)
* add celery, redis (if necessary)
