import os
from uuid import uuid4

from django.utils.deconstruct import deconstructible


class Enum:
    display_names = {}

    @classmethod
    def choices(cls):
        return list(cls.display_names.items())


@deconstructible
class UploadToMediaFolder:
    """
    Класс для загрузки файлов в указанную папку относительно `settings.MEDIA_ROOT`.

    Для использовании следующий образом:
        `field = models.ImageField(upload_to=UploadToMediaFolder(<folder_name>))`

    Параметры:
        folder_name: папка для загрузки относительно `settings.MEDIA_ROOT`
        random_name: если True - файл именуется случайным образом, иначе - остается значение, заданное пользователем.
    """

    def __init__(self, folder_name, random_name=True, *args, **kwargs):
        self.folder_name = folder_name
        self.random_name = random_name

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]

        if self.random_name:
            filename = instance.pk or uuid4().hex

        filename = '{0}.{1}'.format(filename, ext)

        return os.path.join(self.folder_name, filename)
