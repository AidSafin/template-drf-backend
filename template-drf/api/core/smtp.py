import email
import logging
import smtplib
from email.mime.application import MIMEApplication
from os.path import basename

from core import settings


class SMTPClient:
    _source = settings.SMTP_SOURCE
    _from = settings.SMTP_FROM
    _host = settings.SMTP_HOST
    _port = settings.SMTP_PORT
    _password = settings.SMTP_PASSWORD

    _charset = 'utf-8'

    @classmethod
    def send(cls, subject: str, to: list, content: str, files_paths: tuple = None):
        message = email.mime.multipart.MIMEMultipart()
        message['Subject'] = email.header.Header(subject, cls._charset)
        message['From'] = email.header.Header(cls._from, cls._charset)
        message['To'] = email.header.Header(','.join(to), cls._charset)
        for path in files_paths or []:
            try:
                with open(path, 'rb') as file:
                    part = MIMEApplication(file.read(), Name=basename(path))
            except FileNotFoundError:
                pass
            else:
                part['Content-Disposition'] = 'attachment; filename="{filename}"'.format(filename=basename(path))
                message.attach(part)
        message.attach(email.mime.text.MIMEText(content, 'html', _charset=cls._charset))

        try:
            with smtplib.SMTP(cls._host, cls._port) as smtp:
                if all((cls._from, cls._password)):
                    smtp.starttls()
                    smtp.login(cls._from, cls._password)
                smtp.sendmail('<{source}>'.format(source=cls._source), to, message.as_string())
        except smtplib.SMTPAuthenticationError:
            logging.exception('smtp client authorization: invalid username or password')
        except smtplib.SMTPException:
            logging.exception(
                'Error send message from: {from_}, to: {receivers}'.format(
                    from_=cls._from,
                    receivers=','.join(to),
                ))
